// Конструктор Animal
function Animal(name) {
	this.name = name;
}
// Метод getName - в прототип
Animal.prototype.getName = function() {
	return this.name;
}

// Конструктор Dog
function Dog(name){
	this.name = name;
}

//Функция наследования
function extend(Child, Parent) {
	var F = function() { };
	F.prototype = Parent.prototype;
	Child.prototype = new F();
	Child.prototype.constructor = Child;
	Child.superclass = Parent.prototype;
}

extend(Dog, Animal);

// Метод bark - в прототип
Dog.prototype.bark = function() {
	return  'Dog ' + this.name + ' is barking';
}

// var dog = new Dog ('Aban');
// dog.getName() === 'Aban'; // true
// dog.bark () === 'Dog Aban is barking'; // true

var dog = new Dog("Aban");
console.log("dog.getName() === 'Aban'; >> " + (dog.getName() === 'Aban'));
console.log("dog.bark () === 'Dog Aban is barking'; >> " + (dog.bark() === 'Dog Aban is barking'));